@echo off
rem *******************************************************
rem
rem LIBScript-Repair.bat
rem
rem This Script Run LIBScript.SFX.exe file in current
rem directory
rem
rem This Script is Part of NIT.Install.Scripts packets
rem PARAMETERS:	NONE
rem
rem *******************************************************
@echo off

rem Test
rem set EXEPATH=%~dp0
rem set EXEFILE=HelloWorld01.exe
rem set EXEOPT=

rem Install
set EXEPATH=%~dp0
set EXEFILE=LIBScript.SFX.exe
set EXEOPT=-p1234

"%EXEPATH%%EXEFILE%" %EXEOPT%
